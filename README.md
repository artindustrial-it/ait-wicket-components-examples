ait-wicket-components-examples
==============================

About:
------

This are examples of the use of artindustrial it Wicket components. 
There should be a page for each component here for lookup / test purposes. 

License:
--------

This software project builds upon Apache Wicket 
and is licensed under the Apache Software Foundation license, version 2.0.

Run:
----

1. just type 'mvn jetty:run' in your command prompt

Adding new components:
----------------------

1. Create a Page class in com.ait.pages.some-subpackage
2. Insert you component with some example data / usage
3. Start the server and test (it will be added automagically 
   in the menu on the start page)


Comments:
---------

see also 'ait-wicket-components'