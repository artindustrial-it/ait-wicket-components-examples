package com.ait;

import java.util.ArrayList;
import java.util.Set;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.util.ListModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.ait.wicket.component.menu.BookmarkablePageMenuItem;
import com.ait.wicket.component.menu.MenuItem;
import com.ait.wicket.component.menu.MenuPanel;

public class ExamplesPage extends WebPage {
	private static final long serialVersionUID = 1L;
	private ListModel<MenuItem<?>> model;

	public ExamplesPage(final PageParameters parameters) {
		super(parameters);
	}

	protected void onInitialize() {
		super.onInitialize();
		model = new ListModel<MenuItem<?>>(convert(ExamplesApplication.get().getPages()));
		add(new MenuPanel("panel.menu", model));
	}

	private ArrayList<MenuItem<?>> convert(Set<Class<? extends WebPage>> pages) {
		ArrayList<MenuItem<?>> result = new ArrayList<MenuItem<?>>(pages.size());
		for (Class<? extends WebPage> page : pages) {
			result.add(new BookmarkablePageMenuItem(page));
		}
		return result;
	}
}
