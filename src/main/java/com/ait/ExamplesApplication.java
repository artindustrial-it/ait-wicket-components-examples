package com.ait;

import java.util.Set;

import org.apache.wicket.Application;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.reflections.Reflections;
import org.wicketstuff.annotation.scan.AnnotatedMountList;
import org.wicketstuff.annotation.scan.AnnotatedMountScanner;

/**
 * Application object for your web application. If you want to run this
 * application without deploying, run the Start class.
 * 
 * @see com.ait.Start#main(String[])
 */
public class ExamplesApplication extends WebApplication {
	private static final String COM_AIT_PAGES = "com.ait.pages";
	private static Set<Class<? extends WebPage>> pages;

	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage() {
		return ExamplesPage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init() {
		super.init();
		AnnotatedMountList scanPackage = new AnnotatedMountScanner()
				.scanPackage(COM_AIT_PAGES);
		scanPackage.mount(this);
		buildMenu();
	}

	private void buildMenu() {
		if (pages == null) {
			synchronized (this) {
				if (pages == null) {
					Reflections reflections = new Reflections(COM_AIT_PAGES);
					pages = reflections.getSubTypesOf(WebPage.class);
				}
			}
		}
	}

	public static ExamplesApplication get() {
		Application application = Application.get();
		if (application instanceof ExamplesApplication) {
			return (ExamplesApplication) application;
		}
		throw new IllegalStateException("Application is not of type "
				+ ExamplesApplication.class.getSimpleName());
	}

	public Set<Class<? extends WebPage>> getPages() {
		if (pages == null) {
			buildMenu();
		}
		return pages;
	}
}
