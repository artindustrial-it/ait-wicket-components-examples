package com.ait.pages.form;

import org.apache.wicket.markup.html.WebPage;

import com.ait.wicket.component.form.datepicker.BootstrapDatePicker;
import com.ait.wicket.component.form.datepicker.BootstrapDatePicker.WeekStart;
import com.ait.wicket.component.form.datepicker.DatePickerLanguageOption;
import com.ait.wicket.component.form.datepicker.DatePickerOption;

public class FormComponentsPage extends WebPage {

	@Override
	protected void onInitialize() {
		super.onInitialize();
		BootstrapDatePicker datePicker = new BootstrapDatePicker("datepicker1");
		add(datePicker);
		datePicker.setLanguageSettings(DatePickerLanguageOption.DE);
		datePicker.setLanguage("de");
		datePicker.setDateFormat("dd.mm.yyyy");
		datePicker.setWeekStart(WeekStart.MONDAY);
		datePicker.setAutoclose(true);
//		datePicker.setLoadBootstrapJavascript(false);
//		datePicker.setLoadCss(false);

		BootstrapDatePicker datePicker2 = new BootstrapDatePicker(
				"datepicker2");
//		datePicker2.setLoadBootstrapJavascript(false);
//		datePicker2.setLoadCss(false);
		add(datePicker2);
		datePicker2.setOptionSettings(
				DatePickerOption.getStringOption("format", "dd.mm.yyyy"));
	}
}
