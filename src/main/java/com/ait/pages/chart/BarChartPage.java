package com.ait.pages.chart;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;
import org.wicketstuff.annotation.mount.MountPath;

import com.ait.wicket.component.chart.Bar;
import com.ait.wicket.component.chart.BarChartData;
import com.ait.wicket.component.chart.BarChartPanel;
import com.ait.wicket.component.chart.BarSequence;
import com.ait.wicket.component.chart.option.BarChartSettings;
import com.ait.wicket.component.chart.option.XaxisSettings;
import com.ait.wicket.component.chart.option.YaxisSettings;

@MountPath
public class BarChartPage extends WebPage {
	private static final long serialVersionUID = 1L;

	@Override
	protected void onInitialize() {
		super.onInitialize();
		add(new BarChartPanel("panel.chart", new Model<BarChartData>() {
			private static final long serialVersionUID = 1L;

			@Override
			public BarChartData getObject() {
				return getFilteredData();
			}
		}, new XaxisSettings().setMin(0L).setAutoscaleMargin(1).setTickFormatter("function (tick) {"
				+ "if (tick == 0) { return 'Norwegen';}"
				+ "else if (tick == 1) { return 'Schweden';}"
				+ "else if (tick == 2) { return 'Land';}"
				+ "else { return '';}"
				+ "}") ,
		new YaxisSettings().setMin(0L),
		new BarChartSettings().setBarWidth(0.5f))
		);
	}

	protected BarChartData getFilteredData() {
		// @formatter:off
		return new BarChartData(new BarSequence[] {
  			new BarSequence("Männer", new Bar[] {
	  			new Bar(0f, 14f),
	  			new Bar(1f, 14f),
	  			new Bar(2f, 14f),
	  			new Bar(3f, 12f)
  			}),
  			new BarSequence("Frauen", new Bar[] {
  					new Bar(0.2f, 16f),
  					new Bar(1.2f, 15f),
  					new Bar(2.2f, 14f),
  					new Bar(3.2f, 13f)
  			})
  		});
		// @formatter:on
	}
}
