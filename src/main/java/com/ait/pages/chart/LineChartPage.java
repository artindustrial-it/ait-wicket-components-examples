package com.ait.pages.chart;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;
import org.wicketstuff.annotation.mount.MountPath;

import com.ait.wicket.component.chart.FloatCoordinates;
import com.ait.wicket.component.chart.FloatLine;
import com.ait.wicket.component.chart.FloatLineChartData;
import com.ait.wicket.component.chart.FloatLineChartPanel;

@MountPath
public class LineChartPage extends WebPage {
	private static final long serialVersionUID = 1L;

	@Override
	protected void onInitialize() {
		super.onInitialize();
		add(new FloatLineChartPanel("panel.chart", new Model<FloatLineChartData>() {
			private static final long serialVersionUID = 1L;

			@Override
			public FloatLineChartData getObject() {
				return getFilteredData();
			}
		}));
	}

	protected FloatLineChartData getFilteredData() {
		// @formatter:off
  	return new FloatLineChartData(new FloatLine[] {
  			new FloatLine("cool Drama Movies", new FloatCoordinates[] {
  					new FloatCoordinates(2005f, 10f),
  					new FloatCoordinates(2006f, 8f),
  					new FloatCoordinates(2007f, 9f),
  					new FloatCoordinates(2008f, 15f),
  					new FloatCoordinates(2009f, 18f),
  					new FloatCoordinates(2010f, 6f),
  					new FloatCoordinates(2011f, 9f),
  					new FloatCoordinates(2012f, 23f),
  					new FloatCoordinates(2013f, 17f),
  					new FloatCoordinates(2014f, 22f)
  			}),
  			new FloatLine("cool Sci Fi Movies", new FloatCoordinates[] {
  					new FloatCoordinates(2005f, 3f),
  					new FloatCoordinates(2006f, 7f),
  					new FloatCoordinates(2007f, 5f),
  					new FloatCoordinates(2008f, 6f),
  					new FloatCoordinates(2009f, 10f),
  					new FloatCoordinates(2010f, 9f),
  					new FloatCoordinates(2011f, 11f),
  					new FloatCoordinates(2012f, 14f),
  					new FloatCoordinates(2013f, 13f),
  					new FloatCoordinates(2014f, 20f)
  			}),
  			new FloatLine("Action", new FloatCoordinates[] {
  					new FloatCoordinates(2005f, 30f),
  					new FloatCoordinates(2006f, 37f),
  					new FloatCoordinates(2007f, 33f),
  					new FloatCoordinates(2008f, 38f),
  					new FloatCoordinates(2009f, 41f),
  					new FloatCoordinates(2010f, 40f),
  					new FloatCoordinates(2011f, 42f),
  					new FloatCoordinates(2012f, 37f),
  					new FloatCoordinates(2013f, 46f),
  					new FloatCoordinates(2014f, 43f)
  			})
  	});
		// @formatter:on
	}
}
