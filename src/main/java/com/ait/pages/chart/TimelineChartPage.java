package com.ait.pages.chart;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.wicketstuff.annotation.mount.MountPath;

import com.ait.date.DateUtils;
import com.ait.wicket.component.chart.TimeCoordinates;
import com.ait.wicket.component.chart.Timeline;
import com.ait.wicket.component.chart.TimelineChartData;
import com.ait.wicket.component.chart.TimelineChartPanel;
import com.ait.wicket.component.chart.option.GridSettings;
import com.ait.wicket.component.chart.option.LegendSettings;
import com.ait.wicket.component.chart.option.LineChartSettings;
import com.ait.wicket.component.chart.option.MarkerSettings;
import com.ait.wicket.component.chart.option.PointsSettings;
import com.ait.wicket.component.chart.option.XaxisSettings;
import com.ait.wicket.component.chart.option.YaxisSettings;

@MountPath
public class TimelineChartPage extends WebPage {
	private static final String PANEL_OUTSIDE_ID = "panel.outside";
	private static final long serialVersionUID = 1L;

	@Override
	protected void onInitialize() {
		super.onInitialize();
		add(new TimelineChartPanel("panel.chart", new Model<TimelineChartData>() {
			private static final long serialVersionUID = 1L;

			@Override
			public TimelineChartData getObject() {
				return getFilteredData();
			}
		}, 
		new LegendSettings().setPosition("se").setBackgroundColor("#aaddff"),
		new PointsSettings(),
		new LineChartSettings(),
		new XaxisSettings().setAutoscale(true),
		new YaxisSettings().setAutoscale(true).setShowLabels(true),
		new GridSettings().setVerticalLines(true).setBackgroundFunction("{ colors: [[0, '#ffffff'], [1, '#aaaaaa']], "+
					"start: 'top', end: 'bottom' }"),
		new MarkerSettings().setPosition("tc")
		 //
		));

		add(new Label(PANEL_OUTSIDE_ID).setOutputMarkupId(true).setMarkupId(
				PANEL_OUTSIDE_ID));
	}

	protected TimelineChartData getFilteredData() {
		// @formatter:off
  	return new TimelineChartData(new Timeline[] {
  			new Timeline("cool Drama Movies", new TimeCoordinates[] {
  					new TimeCoordinates(DateUtils.parseDate("2005", "yyyy"), 10f),
  					new TimeCoordinates(DateUtils.parseDate("2006", "yyyy"), 8f),
  					new TimeCoordinates(DateUtils.parseDate("2007", "yyyy"), 9f),
  					new TimeCoordinates(DateUtils.parseDate("2008", "yyyy"), 15f),
  					new TimeCoordinates(DateUtils.parseDate("2009", "yyyy"), 18f),
  					new TimeCoordinates(DateUtils.parseDate("2010", "yyyy"), 6f),
  					new TimeCoordinates(DateUtils.parseDate("2011", "yyyy"), 9f),
  					new TimeCoordinates(DateUtils.parseDate("2012", "yyyy"), 23f),
  					new TimeCoordinates(DateUtils.parseDate("2013", "yyyy"), 17f),
  					new TimeCoordinates(DateUtils.parseDate("2014", "yyyy"), 22f)
  			}),
  			new Timeline("cool Sci Fi Movies", new TimeCoordinates[] {
  					new TimeCoordinates(DateUtils.parseDate("2005", "yyyy"), 3f),
  					new TimeCoordinates(DateUtils.parseDate("2006", "yyyy"), 7f),
  					new TimeCoordinates(DateUtils.parseDate("2007", "yyyy"), 5f),
  					new TimeCoordinates(DateUtils.parseDate("2008", "yyyy"), 6f),
  					new TimeCoordinates(DateUtils.parseDate("2009", "yyyy"), 10f),
  					new TimeCoordinates(DateUtils.parseDate("2010", "yyyy"), 9f),
  					new TimeCoordinates(DateUtils.parseDate("2011", "yyyy"), 11f),
  					new TimeCoordinates(DateUtils.parseDate("2012", "yyyy"), 14f),
  					new TimeCoordinates(DateUtils.parseDate("2013", "yyyy"), 13f),
  					new TimeCoordinates(DateUtils.parseDate("2014", "yyyy"), 20f)
  			}),
  			new Timeline("Action", new TimeCoordinates[] {
  					new TimeCoordinates(DateUtils.parseDate("2005", "yyyy"), 30f),
  					new TimeCoordinates(DateUtils.parseDate("2006", "yyyy"), 37f),
  					new TimeCoordinates(DateUtils.parseDate("2007", "yyyy"), 33f),
  					new TimeCoordinates(DateUtils.parseDate("2008", "yyyy"), 38f),
  					new TimeCoordinates(DateUtils.parseDate("2009", "yyyy"), 41f),
  					new TimeCoordinates(DateUtils.parseDate("2010", "yyyy"), 40f),
  					new TimeCoordinates(DateUtils.parseDate("2011", "yyyy"), 42f),
  					new TimeCoordinates(DateUtils.parseDate("2012", "yyyy"), 37f),
  					new TimeCoordinates(DateUtils.parseDate("2013", "yyyy"), 46f),
  					new TimeCoordinates(DateUtils.parseDate("2014", "yyyy"), 43f)
  			})
  	});
		// @formatter:on
	}
}
