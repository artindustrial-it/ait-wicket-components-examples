package com.ait.pages.chart;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;
import org.wicketstuff.annotation.mount.MountPath;

import com.ait.wicket.component.chart.PieChartData;
import com.ait.wicket.component.chart.PieChartPanel;
import com.ait.wicket.component.chart.Slice;

@MountPath
public class PieChartPage extends WebPage {
	private static final long serialVersionUID = 1L;

	@Override
	protected void onInitialize() {
		super.onInitialize();
		add(new PieChartPanel("panel.chart", new Model<PieChartData>() {
			private static final long serialVersionUID = 1L;

			@Override
			public PieChartData getObject() {
				return getFilteredData();
			}
		}));
	}

	protected PieChartData getFilteredData() {
		// @formatter:off
		return new PieChartData(new Slice[] {
				new Slice("Drama", 22f),
				new Slice("Horror", 13f),
				new Slice("Action", 51f),
				new Slice("Romance", 8f),
				new Slice("Sci Fi", 38f),
				new Slice("Fantasy", 2f)
		});
		// @formatter:on
	}
}
