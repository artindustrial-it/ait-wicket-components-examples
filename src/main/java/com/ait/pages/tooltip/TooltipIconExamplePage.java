package com.ait.pages.tooltip;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;

import com.ait.wicket.component.tooltip.IconPanel;
import com.ait.wicket.component.tooltip.IconSettings;

public class TooltipIconExamplePage extends WebPage {

	@Override
	protected void onInitialize() {
		super.onInitialize();
		IconPanel t1 = new IconPanel("tooltip", Model.of("Tooltip Title"), Model.of("Tooltip Content"));
		add(t1);
		IconPanel t2 = new IconPanel("tooltipWithLink", Model.of("Tooltip Title"), Model.of("Tooltip Content"),
				Model.of(new IconSettings("icon-link", "/")));
		add(t2);
	}
}
