package com.ait.pages.test;

import java.util.Arrays;
import java.util.List;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.util.ListModel;
import org.wicketstuff.annotation.mount.MountPath;

@MountPath
public class AppendingJavaScriptToLinkPage extends WebPage {
	private static final long serialVersionUID = 1L;

	@Override
	protected void onInitialize() {
		super.onInitialize();
		IModel<List<String>> model = new ListModel<String>(getList());
		RepeatingView repeatringView = new RepeatingView("repeater", model);
		add(repeatringView);
		for (String item : model.getObject()) {
			WebMarkupContainer wmc = new WebMarkupContainer(repeatringView.newChildId());
			repeatringView.add(wmc);
			String modalMarkupId = "modalId_" + wmc.getId();
			wmc.add(new WebMarkupContainer("link").add(new AttributeAppender("onclick", "alert('"+item + " - " + modalMarkupId + "'); document.getElementById('"+modalMarkupId+"').style.display = 'block';")));
			wmc.add(new WebMarkupContainer("modal").setOutputMarkupId(true).setMarkupId(modalMarkupId));
		}
	}

	private List<String> getList() {
		return Arrays.asList(new String[] {
				"Eintrag 1",
				"Eintrag 2",
				"Eintrag 3"
		});
	}
}
